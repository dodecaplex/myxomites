package myxomites;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import jig.Vector;

public class Flock {
	public static final ArrayList<Flock> instances = new ArrayList<>();
	
	public ArrayList<Boid> members;
	public ArrayList<Boid> dead_members;
	
	public boolean disbanded = false;

	public float separation_radius;
	public float alignment_radius;
	public float cohesion_radius;
	
	public float separation_weight;
	public float alignment_weight;
	public float cohesion_weight;
	
	public float max_speed;
	public float max_force;
	
	public Vector destination;
	public float destination_radius;
	public float destination_weight;
	
	public boolean wrap;
	public float wrap_padding;
	
	public Color col = new Color(160, 96, 0);
	public int t = 0;
	
	public Flock() {
		Flock.instances.add(this);
		members = new ArrayList<>();
		dead_members = new ArrayList<>();
		
		separation_radius = 50.0f;
		alignment_radius = 100.0f;
		cohesion_radius = 100.0f;

		separation_weight = 1.0f;
		alignment_weight = 1.0f;
		cohesion_weight = 1.0f;
		
		max_speed = 4.0f;
		max_force = 1.0f/60;
		
		destination = null;
		destination_radius = 50.0f;
		destination_weight = 1.0f;
		
		wrap = false;
		wrap_padding = 64.0f;
	}
	
	public void update() {
		for (Boid b : members) {
			if (b != null)
				b.update(this);
		}
		
		for (Boid b : dead_members) {
			members.remove(b);
		}
		
		++t;
	}
	
	public void draw(Graphics g) {
		g.setColor(col);
		for (Boid b : members) {
			if (b != null)
				b.draw(g);
		}
	}

	public static Flock create(float sep, float ali, float coh, float speed, float force) {
		Flock f = new Flock();
		instances.add(f);
		f.separation_radius = sep;
		f.alignment_radius = ali;
		f.cohesion_radius = coh;
		f.max_speed = speed;
		f.max_force = force / 60;
		return f;
	}

	public Boid spawn(Vector position, Vector velocity) {
		Boid b = new Boid();
		b.active = true;
		b.setPosition(position);
		b.velocity = velocity;
		b.accel = new Vector(0,0);
		b.hp = 1;
		b.radius = 15;
		members.add(b);
		return b;
	}
	
	public void disband() {
		disbanded = true;
	}
	
	
}
