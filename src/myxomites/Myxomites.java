package myxomites;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.state.StateBasedGame;

import jig.Entity;
import jig.ResourceManager;
import jig.Vector;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

public class Myxomites extends StateBasedGame {

	public static final int WIN_W = 960; // entire game window
	public static final int WIN_H = 720;
	public static final Rectangle WIN_RECT = new Rectangle(WIN_W, WIN_H);
	public static final int SC_W = 720; // playable screen area
	public static final int SC_H = 720;
	public static final Rectangle SC_RECT = new Rectangle(SC_W, SC_H);
	public static final Vector SC_C = new Vector(SC_W/2, SC_H/2);
	public static final int SC_R = SC_W/2;
	
	public static final Stack<Pattern> flow = new Stack<>();
	public static final ArrayList<Pattern> stages = new ArrayList<>();
	
	public static enum Audio {
		DIE, GRAZE, HIT, SHOOT
	}
	
	public static final HashMap<Audio, String> res_audio = new HashMap<>();
	
	public static int score = 0;
	public static int print_score = 0;
	public static int lives = 3;
	public static int stage = 0;
	
	public static enum State {
		INIT,
		MENU,
		STAGE,
		CLEAR,
		GAMEOVER,
		QUIT
	}
	
	public Myxomites(String name) {
		super(name);
		Entity.setCoarseGrainedCollisionBoundary(Entity.CIRCLE);
	}

	@Override
	public void initStatesList(GameContainer container) throws SlickException {
		addState(new MenuState());
		addState(new StageState());
		addState(new ClearState());
		addState(new GameOverState());
		
		stages.add(new Stage1());
		
		res_audio.put(Audio.DIE, "res/myxo_die.wav");
		res_audio.put(Audio.GRAZE, "res/myxo_graze.wav");
		res_audio.put(Audio.HIT, "res/myxo_hit.wav");
		res_audio.put(Audio.SHOOT, "res/myxo_shoot.wav");
		
		for (String v : res_audio.values())
			ResourceManager.loadSound(v);
	}

	public static void main(String[] args) {
		AppGameContainer app;
		try {
			app = new AppGameContainer(new Myxomites(":: myxomites ::"));
			app.setDisplayMode(WIN_W, WIN_H, false);
			app.setVSync(true);
			app.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}

	}
	
	public static Sound getAudio(Audio a) {
		return ResourceManager.getSound(Myxomites.res_audio.get(a));
	}

}
