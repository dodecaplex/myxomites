package myxomites;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class StageState extends BasicGameState {

	public int t;

	public StageState() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {

	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game) {
		t = 0;
		Bullet.populate();
		Boss.populate();
		Spark.populate();
		PlayerBullet.populate();
		Flock.instances.clear();
		
		Myxomites.flow.clear();
		Player.instance.setPosition(Player.startpos);

		if (Myxomites.stage > Myxomites.stages.size()) Myxomites.stage = 0;
		Myxomites.flow.push(Myxomites.stages.get(Myxomites.stage));
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		g.setColor(new Color(255,240,128));
		g.fillRect(0, 0, Myxomites.WIN_W, Myxomites.WIN_H);
		
		Player.instance.draw(g);
		
		for (PlayerBullet b : PlayerBullet.instances) {
			b.draw(g);
		}
		
		for (Flock f : Flock.instances) {
			f.draw(g);
		}
		
		for (Bullet b : Bullet.instances) {
			b.draw(g);
		}
		
		for (Boss b : Boss.instances) {
			b.draw(g);
		}
		
		for (Spark s : Spark.instances) {
			s.draw(g);
		}
		
		g.setColor(new Color(64, 32, 0));
		g.fillRect(Myxomites.SC_W+1, 0, Myxomites.WIN_W, Myxomites.WIN_H);
		
		g.setColor(new Color(255, 255, 128));
		if (Myxomites.print_score < Myxomites.score) {
			if (Myxomites.score - Myxomites.print_score < 20) { ++Myxomites.print_score; }
			else { Myxomites.print_score = (int)Util.lerp(Myxomites.print_score, Myxomites.score, 0.5f); }
		}
		g.drawString(""+Myxomites.print_score, Myxomites.SC_W + 16, 16);

		g.setColor(new Color(255, 128, 255));
		g.drawString(""+Myxomites.lives, Myxomites.SC_W + 16, 48);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		Input input = container.getInput();
		Player.instance.update(input);
		
		if (!Myxomites.flow.isEmpty()) {
			if (Myxomites.flow.peek().update()) {
				Myxomites.flow.pop();
			}
		} else {
			game.enterState(Myxomites.State.CLEAR.ordinal());
		}
		
		for (Bullet b : Bullet.instances) {
			b.update();
		}
		
		for (PlayerBullet b : PlayerBullet.instances) {
			b.update();
		}
		
		for (Flock f : Flock.instances) {
			f.update();
		}
		
		for (Boss b : Boss.instances) {
			b.update();
		}
		
		for (Spark s : Spark.instances) {
			s.update();
		}	
		
		if (Myxomites.stage < 0) {
			game.enterState(Myxomites.State.GAMEOVER.ordinal());
		}

		++t;
		++PlayerBullet.tick;
	}

	@Override
	public int getID() {
		return Myxomites.State.STAGE.ordinal();
	}

}
