package myxomites;

import java.util.ArrayList;

import org.newdawn.slick.Color;

import jig.Vector;

public final class Stage1 extends Pattern {

	@Override
	public boolean proc() {

		switch (t) {
		case 0: Myxomites.flow.push(new Wave0()); break;
		case 1: Myxomites.flow.push(new Wave1()); break;
		case 2: Myxomites.flow.push(new Wave0()); break;
		case 3: Myxomites.flow.push(new Wave2()); break;
		case 4: Myxomites.flow.push(new Wave0()); break;
		case 5: Myxomites.flow.push(new Wave1()); break;
		case 6: Myxomites.flow.push(new Wave3()); break;
		case 7: Myxomites.flow.push(new Wave0()); break;
		case 8: Myxomites.flow.push(new Wave4()); break;
		default: return true;
		}

		return false;
	}
	
	private static class Wave0 extends Pattern {
		private ArrayList<Flock> f0 = new ArrayList<>();
		private ArrayList<Flock> f1 = new ArrayList<>();
		private Vector v0_0 = new Vector(0.25f*Myxomites.SC_W, -64);
		private Vector v0_1 = new Vector(0.15f*Myxomites.SC_W, 0.5f*Myxomites.SC_H);
		private Vector v1_0 = new Vector(0.75f*Myxomites.SC_W, -64);
		private Vector v1_1 = new Vector(0.85f*Myxomites.SC_W, 0.5f*Myxomites.SC_H);
		
		public Wave0() { t_max = 10*60; }

		@Override public boolean proc() {
			if (t < 600 && t % 60 == 0) {
				Flock f = Flock.create(200, 200, 200, 1, 1);
				f.destination_weight = 0.5f;
				f0.add(f);
				for (int i = 0; i < 10; ++i) {
					f.spawn(v0_0.add(Vector.getRandomXY(-128,128,0,0)), Vector.getRandom(0, 1));
				}
				
				f = Flock.create(200, 200, 200, 1, 1);
				f.destination_weight = 0.5f;
				f1.add(f);
				for (int i = 0; i < 10; ++i) {
					f.spawn(v1_0.add(Vector.getRandomXY(-128,128,0,0)), Vector.getRandom(0, 1));
				}
			}
			
			for (Flock f : f0) {
				f.destination = Util.lerp(v0_0, v0_1, Util.saturate(f.t / (8.0f*60)));
				if (f.t > 9*60) f.disband(); 
			}
			
			for (Flock f : f1) {
				f.destination = Util.lerp(v1_0, v1_1, Util.saturate(f.t / (8.0f*60)));
				if (f.t > 9*60) f.disband(); 
			}

			return false;
		}
	}
	
	private static class Wave1 extends Pattern {
		private Boss b0 = Boss.spawn(new Vector(Myxomites.SC_R, -64), 100.0f);
		
		@Override public boolean proc() {
			if (b0.active) {
				if (t%120 == 0) b0.move(Myxomites.SC_C.add(Vector.getRandom(128.0f, 256.0f)), 2.0f, 1.0f);
				
				if (t%120 > 90 && t%3==0) {
					Vector v = Util.pdir(b0.getPosition());
					Bullet b = Bullet.fire(b0.getPosition(), v.scale(2.5f));
					b.radius = Util.rand(8, 12);
				}
			} else {
				return true;
			}
			
			return false;
		}
	}
	
	private static class Wave2 extends Pattern {
		private Boss b0 = Boss.spawn(new Vector(Myxomites.SC_R, -64), 100.0f);
		private Flock f0 = Flock.create(200, 200, 400, 1, 1);
		private boolean flag = true;
		
		public Wave2() {
			for (int i = 0; i < 32; ++i) {
				Boid b = f0.spawn(b0.getPosition(), Vector.getRandom(1));
				b.radius = 12;
			}
		}
		
		@Override public boolean proc() {
			if (b0.active) {
				if (t > 60 && t%120 == 0) {
					b0.move(Util.ppos(), 2.5f, 0.8f);
					for (int i = 0; i < 16; ++i) {
						Bullet.fire(b0.getPosition(), Vector.getUnit(Util.PHI*b0.t).rotate(Util.ang(i, 16)).scale(1.5f));
						Boid b = f0.spawn(b0.getPosition(), Vector.getRandom(1));
						b.radius = 12;
					}
				}
				
				f0.destination = b0.getPosition();
			} else {
				f0.destination = Util.ppos();
				if (flag) {
					for (int i = 0; i < 64; ++i) {
						Boid b = f0.spawn(Myxomites.SC_C.add(Vector.getRandom(Util.RT2*(32+Myxomites.SC_R))), Vector.getRandom(1));
						b.radius = 8;
					}
					t_max = t + 600;
					flag = false;
				}
			}
			return false;
		}
	}
	
	private static class Wave3 extends Pattern {
		private Boss b0 = Boss.spawn(new Vector(-100, Myxomites.SC_R), 100.0f);
		private Boss b1 = Boss.spawn(new Vector(Myxomites.SC_W + 100, Myxomites.SC_R), 100.0f);
		private boolean b0_flag = true;
		private boolean b1_flag = true;
		private Flock f0 = Flock.create(200, 200, 400, 1, 0.5f);
		
		public Wave3() {
			f0.destination = Myxomites.SC_C;
			f0.destination_radius = 0.25f*Myxomites.SC_R;
		}
		
		@Override public boolean proc() {
			if (b0.active) {
				if (t%120 == 0) {
					b0.move(Vector.getRandomXY(0, Myxomites.SC_R-200, 0, Myxomites.SC_H), 3.0f, 2.0f);
				}
				if (t%60==0) {
					for (int i = 0; i < 8; ++i) {
						Bullet.fire(b0.getPosition(), Vector.getUnit(Util.PHI*b0.t).rotate(Util.ang(i, 8)).scale(2));
					}
				}
			} else {
				if (b0_flag) {
					for (int i = 0; i < 50; ++i)
						f0.spawn(Vector.getRandomXY(Myxomites.SC_W, Myxomites.SC_W+128, 0, Myxomites.SC_H), new Vector(-1,0));
					b0_flag = false;
				}
			}
			
			if (b1.active) {
				if (t%120 == 60) {
					b1.move(Vector.getRandomXY(Myxomites.SC_R+200, Myxomites.SC_W, 0, Myxomites.SC_H), 3.0f, 2.0f);
				}
				if (t%60==30) {
					for (int i = 0; i < 8; ++i) {
						Bullet.fire(b1.getPosition(), Vector.getUnit(Util.PHI*b1.t).rotate(Util.ang(i, 8)).scale(2));
					}
				}
			} else {
				if (b1_flag) {
					for (int i = 0; i < 50; ++i)
						f0.spawn(Vector.getRandomXY(-128, 0, 0, Myxomites.SC_H), new Vector(+1,0));
					b1_flag = false;
				}
			}

			return !(b0_flag || b1_flag);
		}
	}
	
	private static class Wave4 extends Pattern {
		private Boss b0 = Boss.spawn(new Vector(Myxomites.SC_R, Myxomites.SC_H + 64), 200.0f);
		private Flock f0 = Flock.create(100, 120, 200, 1.5f, 0.5f);
		
		public Wave4() {
			b0.move(Myxomites.SC_C, 1, 1);
		}
		
		@Override public boolean proc() {
			if (b0.active) {
				if (!b0.mp_enable) {
					b0.setPosition(Util.lerp(b0.getPosition(), Myxomites.SC_C.add(Util.ellipse(100,50,0.125f*Util.PHI*b0.t).rotate(t)), 0.5f));
					if (t%5 == 0) {
						if (t%2==0) {
							Bullet.fire(b0.getPosition(), Util.pdir(b0.getPosition()).scale(1.5f));
							f0.spawn(b0.getPosition(), Vector.getRandom(1.5f));
						}
						Bullet b = Bullet.fire(b0.getPosition(), Vector.getUnit(-t).scale(1.75f));
						b.col0 = new Color(255,96,0);
						b.radius = 14;
						
						if (t > 15*60) {
							b = Bullet.fire(b0.getPosition(), Vector.getRandom(2.0f));
							b.col0 = new Color(255,96,96);
							b.radius = 6;
						}
						
						if (t > 30*60) {
							b = Bullet.fire(b0.getPosition(), Vector.getRandom(1.0f));
							b.col0 = new Color(255,0,128);
							b.radius = 8;
						}
						
						if (t > 60*60) {
							b = Bullet.fire(b0.getPosition(), Vector.getRandom(0.5f));
							b.col0 = new Color(192,96,255);
							b.radius = 10;
						}
						
						if (t > 90*60) {
							b = Bullet.fire(b0.getPosition(), Vector.getRandom(3.0f));
							b.col0 = new Color(96,255,0);
							b.radius = 16;
						}
					}
				}
				t_max = t + 300;
			} else {
				f0.disband();
			}
			return false;
		}
		
	}

}
