package myxomites;

import jig.Vector;

public final class Util {
	public static final float PI = (float)Math.PI;
	public static final float TAU = (float)Math.PI * 2f;
	public static final float PHI = (float)((1+Math.sqrt(5)) / 2.0f);
	public static final float RT2 = (float)Math.sqrt(2);
	
	public static float lerp(float a, float b, float t) {
		return (1.0f - t) * a + t * b;
	}
	
	public static Vector lerp(Vector a, Vector b, float t) {
		return new Vector(lerp(a.getX(), b.getX(), t),
				          lerp(a.getY(), b.getY(), t));
	}
	
	public static float min(float a, float b) { return a <= b ? a : b; }
	public static float max(float a, float b) { return a >= b ? a : b; }
	
	public static float clamp(float x, float min, float max) {
	    if (x < min) x = min;
	    if (x > max) x = max;
	    return x;
	}
	
	public static float saturate(float x) {
		return clamp(x, 0, 1);
	}
	
	public static float smoothstep(float a, float b, float t) {
	    t = saturate((t - a)/(b - a)); 
	    return t*t*(3.0f - 2.0f*t);
	}
	
	public static Vector smoothstep(Vector a, Vector b, float t) {
		return new Vector(smoothstep(a.getX(), b.getX(), t),
				          smoothstep(a.getY(), b.getY(), t));
	}
	
	public static float osc(float t, float min, float max) {
		final float range = 0.5f*(max-min);
		return (min + range) + range * (float)Math.sin(t);
	}
	
	public static float ang(int i, float t) {
		return i*(360/t);
	}
	
	public static Vector ellipse(float rx, float ry, float theta) {
		theta /= TAU;
		return new Vector(rx*(float)Math.cos(theta), ry*(float)Math.sin(theta));
	}

	public static float rand(float min, float max) {
		return min + (max-min)*(float)Math.random();
	}
	
	public static Vector ppos() {
		return Player.instance.getPosition();
	}
	
	public static Vector pdir(Vector p) {
		return ppos().subtract(p).unit();
	}
	

}
