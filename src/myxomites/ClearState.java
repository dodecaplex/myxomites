package myxomites;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class ClearState extends BasicGameState {

	public int t;
	public String clear_str;

	public ClearState() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game) {
		t = 0;
		clear_str = ":: STAGE " + (Myxomites.stage+1) + " CLEAR ::";
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		g.setColor(new Color(32, 16, 0));
		g.fillRect(0, 0, Myxomites.WIN_W, Myxomites.WIN_H);
		
		g.setColor(new Color(255,255,255));
		g.drawString(clear_str, Myxomites.SC_R - g.getFont().getWidth(clear_str)/2, Myxomites.SC_R);
		
		g.setColor(new Color(64, 32, 0));
		g.fillRect(Myxomites.SC_W+1, 0, Myxomites.WIN_W, Myxomites.WIN_H);

		g.setColor(new Color(255, 255, 128));
		if (Myxomites.print_score < Myxomites.score) {
			if (Myxomites.score - Myxomites.print_score < 20) { ++Myxomites.print_score; }
			else { Myxomites.print_score = (int)Util.lerp(Myxomites.print_score, Myxomites.score, 0.5f); }
		}
		g.drawString(""+Myxomites.print_score, Myxomites.SC_W + 16, 16);

		g.setColor(new Color(255, 128, 255));
		g.drawString(""+Myxomites.lives, Myxomites.SC_W + 16, 48);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		if (t > 300) {
			++Myxomites.stage;
			if (Myxomites.stage >= Myxomites.stages.size()) {
				game.enterState(Myxomites.State.GAMEOVER.ordinal());
			} else {
				game.enterState(Myxomites.State.STAGE.ordinal());
			}
		}
		
		if (Myxomites.print_score == Myxomites.score) {
			++t;
		}
	}

	@Override
	public int getID() {
		return Myxomites.State.CLEAR.ordinal();
	}

}
