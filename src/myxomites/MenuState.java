package myxomites;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class MenuState extends BasicGameState {
	
	private int select;
	private final int menu_items = 2;
	private final String title_str = ":: myxomites ::";
	private final String start_str = "start";
	private final String quit_str = "quit";

	public MenuState() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {

	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game) {
		select = 0;
		Myxomites.print_score = Myxomites.score = 0;
		Myxomites.lives = 3;
		Myxomites.stage = 0;
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		g.setColor(new Color(255,240,128));
		g.fillRect(0, 0, Myxomites.WIN_W, Myxomites.WIN_H);
		g.setColor(new Color(64, 32, 0));
		g.drawString(title_str, (Myxomites.WIN_W - g.getFont().getWidth(title_str))/2, Myxomites.WIN_H/2 - 32);
		final int sw = g.getFont().getWidth("> ");
		g.drawString(select == 0 ? "> "+start_str : start_str, (Myxomites.WIN_W - (g.getFont().getWidth(start_str) + select==0?sw:0))/2 - 32, Myxomites.WIN_H/2);
		g.drawString(select == 1 ? "> "+quit_str : quit_str, (Myxomites.WIN_W - (g.getFont().getWidth(quit_str) + select==1?sw:0))/2 - 32, Myxomites.WIN_H/2+16);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		final Input input = container.getInput();
		
		if (input.isKeyPressed(Input.KEY_S)
		||  input.isKeyPressed(Input.KEY_DOWN)
		) {
			if (++select >= menu_items) { select = 0; }
		} else if (input.isKeyPressed(Input.KEY_W)
			   ||  input.isKeyPressed(Input.KEY_UP)
		) {
			if (--select < 0) { select = menu_items-1; }
		}
		
		if (
		    input.isKeyPressed(Input.KEY_ENTER) ||
		    input.isKeyPressed(Input.KEY_SPACE) ||
		    input.isMousePressed(Input.MOUSE_LEFT_BUTTON)
		) {
			switch (select) {
			case 0: game.enterState(Myxomites.State.STAGE.ordinal()); break;
			default: game.enterState(Myxomites.State.QUIT.ordinal());
			}
		}

	}

	@Override
	public int getID() {
		return Myxomites.State.MENU.ordinal();
	}

}
