package myxomites;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import jig.Entity;
import jig.ResourceManager;
import jig.Vector;

public class Bullet extends Entity {
	public static final int MAX_COUNT = 800;
	public static final ArrayList<Bullet> instances = new ArrayList<>(MAX_COUNT);
	
	public boolean active;
	public float dx;
	public float dy;
	public int t;
	public Color col0 = new Color(255, 160, 0);
	public Color col1 = new Color(255, 240, 64);
	public float radius = 12.0f;
	public boolean grazed = false;

	public void update() {
		if (!active) return;
		if (t >= 0) {
			translate(dx, dy);
		}
		
		if (
			getX() < -radius || getY() < -radius ||
			getX() > Myxomites.SC_W + radius ||
			getY() > Myxomites.SC_H + radius
		) {
			active = false;
		}
		
		{	float pd2 = getPosition().distanceSquared(Player.instance.getPosition());
			float pr2 = Player.instance.radius*Player.instance.radius;
			if (pd2 < pr2 + 2*(radius*radius)) {
				if (!grazed) {
					Myxomites.score += 10;
					Spark.fire(4, getPosition(), col0, radius);
					Myxomites.getAudio(Myxomites.Audio.GRAZE).play(Util.rand(0.25f, 1.75f), 0.125f);
					grazed = true;
				}
				if (pd2 < pr2) {
					if (Player.instance.invincibility == 0) Player.instance.kill();
					active = false;
				}
			}
		}
		
		++t;
	}
	
	public void draw(Graphics g) {
		if (!active) return;
		g.setColor(col0);
		g.fillOval(getX()-radius, getY()-radius, 2*radius, 2*radius, 8);
		g.setColor(col1);
		float r = Util.osc(t/5.0f, 0.4f*radius, 0.6f*radius);
		g.fillOval(getX()-r, getY()-r, 2*r, 2*r, 8);
	}
	
	public static Bullet findInactive() {
		for (Bullet b : Bullet.instances) {
			if (!b.active) return b;
		}
		return null;
	}
	
	public static Bullet fire(Vector position, Vector velocity) {
		Bullet b = findInactive();
		if (b != null) {
			b.active = true;
			b.setPosition(position);
			b.dx = velocity.getX();
			b.dy = velocity.getY();
			b.col0 = new Color(255, 160, 0);
			b.col1 = new Color(255, 240, 64);
			b.radius = 12;
			b.grazed = false;
			b.t = 0;
		}
		return b;
	}
	
	public static void populate() {
		Bullet.instances.clear();
		for (int i = 0; i < Bullet.MAX_COUNT; ++i) {
			Bullet.instances.add(new Bullet());
		}
	}

}
