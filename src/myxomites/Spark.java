package myxomites;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import jig.Entity;
import jig.Vector;

public class Spark extends Entity {
	public static final int MAX_COUNT = 100;
	public static final ArrayList<Spark> instances = new ArrayList<>(MAX_COUNT);
	
	public boolean active;
	public float dx;
	public float dy;
	public Color col = new Color(255, 160, 0);
	public float radius;

	public void update() {
		if (!active) return;
		translate(dx, dy);
		radius = Util.lerp(radius, 0, 0.125f);
		dx = Util.lerp(dx, 0, 0.0125f);
		dy = Util.lerp(dy, 0, 0.0125f);
		if (radius < 2.0f) active = false;
	}
	
	public void draw(Graphics g) {
		if (!active) return;
		g.setColor(col);
		g.fillOval(getX()-radius, getY()-radius, 2*radius, 2*radius, 4);
	}
	
	public static Spark findInactive() {
		for (Spark b : Spark.instances) {
			if (!b.active) return b;
		}
		return null;
	}
	
	public static void fire(int count, Vector position, Color col, float r) {
		for (int i = 0; i < count; ++i) {
			Vector v = Vector.getRandom(Util.rand(0.5f, 1.5f), Util.rand(8.0f, 10.0f));
			Spark b = findInactive();
			if (b != null) {
				b.active = true;
				b.setPosition(position);
				b.dx = v.getX();
				b.dy = v.getY();
				b.col = col;
				b.radius = r * Util.rand(0.75f, 1.5f);
			} else break;
		}
	}
	
	public static void populate() {
		Spark.instances.clear();
		for (int i = 0; i < Spark.MAX_COUNT; ++i) {
			Spark.instances.add(new Spark());
		}
	}

}
