package myxomites;

import java.util.ArrayList;

import org.newdawn.slick.Graphics;

import jig.Entity;
import jig.Vector;

public class PlayerBullet extends Entity {
	public static final int MAX_COUNT = 100;
	public static final ArrayList<PlayerBullet> instances = new ArrayList<>(MAX_COUNT);
	
	public static float radius = 4.0f;
	public static float speed = 12.0f;
	public static int spread = 1;
	public static float spread_angle = 5.0f;
	public static float spread_decay = 1.0f;
	public static int spacing = 10;
	public static int tick = 0;
	public static float damage = 1.0f;
	
	public boolean active;
	public float dx;
	public float dy;

	public void update() {
		if (!active) return;
		translate(dx, dy);
		
		if (
			getX() < -radius || getY() < -radius ||
			getX() > Myxomites.SC_W + radius ||
			getY() > Myxomites.SC_H + radius
		) {
				active = false;
		}
	}
	
	public void draw(Graphics g) {
		if (!active) return;
		g.setColor(Player.col);
		g.fillOval(getX()-radius, getY()-radius, 2*radius, 2*radius, 16);
	}
	
	public static PlayerBullet findInactive() {
		for (PlayerBullet b : PlayerBullet.instances) {
			if (!b.active) return b;
		}
		return null;
	}
	
	public static PlayerBullet fire(Vector position, Vector velocity) {
		PlayerBullet b = findInactive();
		if (b != null) {
			b.active = true;
			b.setPosition(position);
			b.dx = velocity.getX();
			b.dy = velocity.getY();
		}
		return b;
	}
	
	public static void populate() {
		PlayerBullet.instances.clear();
		for (int i = 0; i < PlayerBullet.MAX_COUNT; ++i) {
			PlayerBullet.instances.add(new PlayerBullet());
		}
	}

}
