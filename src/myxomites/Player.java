package myxomites;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import jig.Entity;
import jig.ResourceManager;
import jig.Vector;

public class Player extends Entity {
	public static final Vector startpos = new Vector(Myxomites.SC_W/2.0f, 7.0f*Myxomites.SC_H/8.0f);
	public static final Player instance = new Player(Player.startpos);
	public static final Color col = new Color(196, 0, 80);
	public static final Color flash_col = new Color(255, 0, 160);

	public float speed = 4.0f;
	public float focus_factor = 0.5f;
	public float dx;
	public float dy;
	public float radius = 10.0f;
	public int invincibility = 0;
	

	public Player(Vector position) {
		super(position);
	}

	public Player(float x, float y) {
		super(new Vector(x, y));
	}
	
	public void update(Input input) {
		float sp = speed;
		if (input.isKeyDown(Input.KEY_LSHIFT)) sp *= focus_factor;
		
		if (input.isKeyDown(Input.KEY_A))      dx = -sp;
		else if (input.isKeyDown(Input.KEY_D)) dx = +sp;
		else dx = 0.0f;
		
		if (input.isKeyDown(Input.KEY_W))      dy = -sp;
		else if (input.isKeyDown(Input.KEY_S)) dy = +sp;
		else dy = 0.0f;
		
		if (input.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON) && (PlayerBullet.tick % PlayerBullet.spacing == 0)) {
			Vector v = new Vector(input.getMouseX(), input.getMouseY());
			v = v.subtract(getPosition()).unit();
			PlayerBullet.fire(getPosition(), v.scale(PlayerBullet.speed));
			ResourceManager.getSound(Myxomites.res_audio.get(Myxomites.Audio.SHOOT)).play(1.0f, 0.125f);
			for (int i = 1; i < PlayerBullet.spread+1; ++i) {
				Vector vv = v.scale(PlayerBullet.speed - i*PlayerBullet.spread_decay);
				PlayerBullet.fire(getPosition(), vv.rotate(+i*PlayerBullet.spread_angle));
				PlayerBullet.fire(getPosition(), vv.rotate(-i*PlayerBullet.spread_angle));
			}
			
		}
		
		translate(dx, dy);
		setPosition(getPosition().clamp(Myxomites.SC_RECT));
		
		if (invincibility > 0) --invincibility;
		if (input.isKeyPressed(Input.KEY_TAB)) ++Myxomites.lives;
	}
	
	public void draw(Graphics g) {
		g.setColor(invincibility % 8 < 4 ? Player.col : Player.flash_col);
		g.fillOval(getX()-radius, getY()-radius, 2*radius, 2*radius, 16);
	}
	
	public void kill() {
		Spark.fire(60, getPosition(), col, radius);
		Myxomites.getAudio(Myxomites.Audio.DIE).play(1.0f, 0.5f);
		if (Myxomites.lives > 0) {
			setPosition(startpos);
			invincibility = 120;
			--Myxomites.lives;
		} else {
			Myxomites.stage = -1;
		}
	}

}
