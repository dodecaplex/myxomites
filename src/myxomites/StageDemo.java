package myxomites;

import jig.Vector;

public final class StageDemo extends Pattern {
	private Flock myflock;
	
	public StageDemo() {
		myflock = Flock.create(200, 220, 300, 1.5f, 0.5f);
		myflock.wrap = false;
		myflock.separation_weight = 1.5f;
		for (int i = 0; i < 100; ++i) {
			myflock.spawn(Vector.getRandomXY(0, Myxomites.SC_W, -256, -16), new Vector(0,0));
		}
	}
	
	@Override
	public boolean proc() {

		if (t % 30 == 0) {
			for (Boid b : myflock.members) {
				final Vector p = b.getPosition();
				if (Math.random() < 0.025) {
					Bullet.fire(p, Player.instance.getPosition().subtract(p).unit().scale(1.5f));
				}
			}
		}
		
		myflock.destination = Player.instance.getPosition();
		
		return false;
	}

}
