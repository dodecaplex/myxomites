package myxomites;

import org.newdawn.slick.Graphics;

import jig.Entity;
import jig.ResourceManager;
import jig.Vector;

public class Boid extends Entity {

	public boolean active;
	public Vector velocity;
	public Vector accel;
	public float hp;
	public float radius;
	
	public void update(Flock flock) {
		if (!active) return;
		
		if (getPosition().distanceSquared(Player.instance.getPosition()) < (Player.instance.radius*Player.instance.radius)
		&& Player.instance.invincibility == 0
		) {
			Player.instance.kill();
		}
		
		if (getPosition().distanceSquared(Myxomites.SC_C) > 4*Myxomites.SC_R*Myxomites.SC_R) {
			flock.dead_members.add(this);
		}
		
		for (PlayerBullet b : PlayerBullet.instances) {
			if (!b.active) continue;
			Vector pos = b.getPosition();
			if (pos.distanceSquared(this.getPosition()) < (radius*radius)) {
				Spark.fire(2, b.getPosition(), Player.col, PlayerBullet.radius);
				b.active = false;
				hp -= PlayerBullet.damage;
				if (hp <= 0) {
					kill(flock);
					return;
				}
			}
		}
		
		Vector s = separation(flock).scale(flock.separation_weight);
		Vector a = alignment(flock).scale(flock.alignment_weight);
		Vector c = cohesion(flock).scale(flock.cohesion_weight);
		
		if (flock.destination != null) {
			accel = flock.destination.subtract(this.getPosition())
					                 .setLength(flock.max_speed)
					                 .subtract(velocity)
					                 .clampLength(0, flock.max_force)
					                 .scale(flock.destination_weight)
					                 .scale(
					                	Util.saturate(4*(getPosition().distance(flock.destination)-flock.destination_radius)/Myxomites.SC_W)
					                 );
		}
		accel = accel.add(s).add(a).add(c);
		velocity = velocity.add(accel).clampLength(0, flock.max_speed);
		translate(velocity);
		
		{
			final float xx = getPosition().getX();
			final float yy = getPosition().getY();
			final float w = Myxomites.SC_W + flock.wrap_padding + radius;
			final float h = Myxomites.SC_H + flock.wrap_padding + radius;
			
			if (flock.disbanded) {
				if (xx < -flock.wrap_padding - radius
				||  xx > w
				||  yy < -flock.wrap_padding - radius
				||  yy > h
				) {
					this.active = false;
					flock.dead_members.add(this);
				}
			} else if (flock.wrap) {
				if (xx < -flock.wrap_padding - radius)
					translate(new Vector(w, 0));
				else if (xx > w)
					translate(new Vector(-w, 0));
				
				if (yy < -flock.wrap_padding - radius)
					translate(new Vector(0, h));
				else if (yy > h)
					translate(new Vector(0, -h));
			}
		}
	}
	
	public void draw(Graphics g) {
		if (!active) return;
		g.fillOval(getX()-radius, getY()-radius, 2*radius, 2*radius, 16);
	}
	
	public Vector separation(Flock flock) {
		Vector sum = new Vector(0,0);
		int count = 0;
		for (Boid other : flock.members) {
			float dist = this.getPosition().distance(other.getPosition());
			if (dist > 0 && dist < flock.separation_radius) {
				Vector repel = this.getPosition().subtract(other.getPosition());
				sum = sum.add(repel.setLength(1/dist));
				++count;
			}
		}
		if (count > 0) {
			sum.scale(1.0f / count);
		}
		if (sum.lengthSquared() > 0) {
			sum = sum.setLength(flock.max_speed)
					 .subtract(velocity)
					 .clampLength(0, flock.max_force);
		}
		return sum;
	}
	
	public Vector alignment(Flock flock) {
		Vector sum = new Vector(0,0);
		int count = 0;
		for (Boid other : flock.members) {
			float dist = this.getPosition().distance(other.getPosition());
			if (dist > 0 && dist < flock.alignment_radius) {
				sum = sum.add(other.velocity);
				++count;
			}
		}
		if (count > 0) {
			return sum.setLength(flock.max_speed)
					  .subtract(velocity)
					  .clampLength(0, flock.max_force);
		}
		return new Vector(0,0);
	}
	
	public Vector cohesion(Flock flock) {
		Vector sum = new Vector(0,0);
		int count = 0;
		for (Boid other : flock.members) {
			float dist = this.getPosition().distance(other.getPosition());
			if (dist > 0 && dist < flock.cohesion_radius) {
				sum = sum.add(other.getPosition());
				++count;
			}
		}
		if (count > 0) {
			return sum.scale(1.0f / count)
					  .subtract(getPosition())
					  .setLength(flock.max_speed)
					  .subtract(velocity)
					  .clampLength(0, flock.max_force);
		}
		return new Vector(0,0);
	}
	
	public void kill(Flock flock) {
		this.active = false;
		Myxomites.score += 10;
		Spark.fire(15, getPosition(), flock.col, radius);
		Myxomites.getAudio(Myxomites.Audio.HIT).play(Util.rand(0.8f, 1.5f), 0.125f);
		flock.dead_members.add(this);
	}
}
