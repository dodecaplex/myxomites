package myxomites;

public abstract class Pattern {
	public int t;
	public int t_max;
	
	public Pattern() {
		this.t_max = 0;
	}
	
	public Pattern(int t_max) {
		this.t_max = t_max;
	}
	
	public boolean update() {
		if (t_max > 0 && t > t_max) return true;
		final boolean result = proc();
		++t;
		return result;
	}
	
	public abstract boolean proc();
}
