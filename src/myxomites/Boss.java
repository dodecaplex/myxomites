package myxomites;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import jig.Entity;
import jig.Vector;

public class Boss extends Entity {
	public static final int MAX_COUNT = 4;
	public static final ArrayList<Boss> instances = new ArrayList<>(MAX_COUNT);

	public boolean active;
	public float max_hp;
	public float hp;
	public float radius = 32.0f;
	public Color col0 = new Color(160, 96, 0);
	public Color col1 = new Color(192, 128, 0);;
	public int t;
	
	// motion planning
	public boolean mp_enable;
	public Vector mp_src;
	public Vector mp_dst;
	public float mp_exp;
	public float mp_rate;
	public float mp_t;
	
	public void move(Vector destination, float speed, float exp) {
		mp_enable = true;
		mp_t = 0;
		mp_src = getPosition();
		mp_dst = destination;
		mp_rate = speed / mp_dst.subtract(mp_src).length();
		mp_exp = exp;
	}
	
	public void update() {
		if (!active) return;
		
		if (getPosition().distanceSquared(Player.instance.getPosition()) < (Player.instance.radius*Player.instance.radius)
		&& Player.instance.invincibility == 0
		) {
			Player.instance.kill();
		}
		
		for (PlayerBullet b : PlayerBullet.instances) {
			if (!b.active) continue;
			Vector pos = b.getPosition();
			if (pos.distanceSquared(this.getPosition()) < (radius*radius)) {
				Spark.fire(2, b.getPosition(), Player.col, PlayerBullet.radius);
				b.active = false;
				hp -= PlayerBullet.damage;
				
				Myxomites.getAudio(Myxomites.Audio.GRAZE).play(Util.rand(0.05f, 0.1f), Util.lerp(0.5f, 0.125f, hp/max_hp));
				
				if (hp <= 0) {
					kill();
					return;
				}
			}
		}
		
		if (mp_enable) {
			float tt = mp_exp > 0.0f ? Util.smoothstep(0, 1, (float)Math.pow(mp_t, mp_exp)) : mp_t;
			setPosition(Util.lerp(mp_src, mp_dst, tt));
			if (mp_t > 1.0f) {
				setPosition(mp_dst);
				mp_enable = false;
			}
			mp_t += mp_rate;
		}
		++t;
	}
	
	public void draw(Graphics g) {
		if (!active) return;
		
		float arc_r = radius*1.25f;
		g.setColor(Color.red);
		g.fillArc(getX()-arc_r, getY()-arc_r, 2*arc_r, 2*arc_r, 0, 360*(hp/max_hp));
		g.setColor(col0);
		g.fillOval(getX()-radius, getY()-radius, 2*radius, 2*radius, 16);
		g.setColor(col1);
		float r = Util.osc(t/5.0f, 0.4f*radius, 0.6f*radius);
		g.fillOval(getX()-r, getY()-r, 2*r, 2*r, 16);
	}
	
	public void kill() {
		this.active = false;
		Myxomites.score += 10 * max_hp;
		Spark.fire(32, getPosition(), col0, radius);
		Spark.fire(32, getPosition(), col1, radius);
		Myxomites.getAudio(Myxomites.Audio.HIT).play(Util.rand(0.4f, 0.8f), 0.5f);
		mp_enable = false;
	}
	
	public static Boss findInactive() {
		for (Boss b : Boss.instances) {
			if (!b.active) return b;
		}
		return null;
	}
	
	public static Boss spawn(Vector position, float max_hp) {
		Boss b = findInactive();
		if (b != null) {
			b.active = true;
			b.setPosition(position);
			b.max_hp = b.hp = max_hp;
			b.t = 0;
		}
		return b;
	}
	
	public static void populate() {
		Boss.instances.clear();
		for (int i = 0; i < Boss.MAX_COUNT; ++i) {
			Boss.instances.add(new Boss());
		}
	}
}
